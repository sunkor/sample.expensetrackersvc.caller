﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Collections.Generic;

namespace Sample.ExpenseTrackerSvc.Caller
{
    class Program
    {
        static void Main(string[] args)
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:1337/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                
                await CallCategories(client);

                //await CallCashItemsCRUD(client);

                await GetCashItemsToSync(client);

                //await SyncCashItemsToServer(client);
            }
        }

        private static async Task GetCashItemsToSync(HttpClient client)
        {
            //Get items to sync based on last date time modified
            var str = DateTime.UtcNow.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
            var response = await client.GetAsync("cashitems/sync/" + str);
            if (response.IsSuccessStatusCode)
            {
                var responseData = await response.Content.ReadAsStringAsync();
                var obj = JsonConvert.DeserializeObject(responseData);
            }
        }

        private static async Task SyncCashItemsToServer(HttpClient client)
        {
            var cashItems = new List<CashItem>();

            cashItems.Add(new CashItem()
            {
                ItemID = 3,
                Date = Convert.ToDateTime("04 Aug 2016"),
                CategoryId = 2,
                Amount = 40.55,
                Description = "Optus Post Paid",
                Deleted = false,
                LastUpdate = DateTime.Now
            });

            cashItems.Add(new CashItem()
            {
                ItemID = 4,
                Date = Convert.ToDateTime("05 Aug 2016"),
                CategoryId = 1,
                Amount = 39.85,
                Description = "Optus Post Paid",
                Deleted = true,
                LastUpdate = DateTime.Now
            });

            //var items = JsonConvert.SerializeObject(cashItems);

            var settings = new JsonSerializerSettings();
            settings.ContractResolver = new LowercaseContractResolver();
            var json = JsonConvert.SerializeObject(cashItems, Formatting.Indented, settings);

            var response = await client.PostAsync("cashitems/sync", new StringContent(json, Encoding.UTF8, "application/json"));
            if (response.IsSuccessStatusCode)
            {

            }
        }

        private static async Task CallCategories(HttpClient client)
        {
            //Get Income categories
            HttpResponseMessage response = await client.GetAsync("categories/incomes");
            if (response.IsSuccessStatusCode)
            {
                var responseData = await response.Content.ReadAsStringAsync();
                var obj = JsonConvert.DeserializeObject(responseData);
            }

            //Get Income categories
            response = await client.GetAsync("categories/expenses");
            if (response.IsSuccessStatusCode)
            {
                var responseData = await response.Content.ReadAsStringAsync();
                var obj = JsonConvert.DeserializeObject(responseData);
            }
        }

        private static async Task CallCashItemsCRUD(HttpClient client)
        {
            //Get Item by ID
            var response = await client.GetAsync("cashitems/1");
            if (response.IsSuccessStatusCode)
            {
                var responseData = await response.Content.ReadAsStringAsync();
                var obj = JsonConvert.DeserializeObject(responseData);
            }

            //Delete
            response = await client.DeleteAsync("cashitems/3");
            if (response.IsSuccessStatusCode)
            {
                var responseData = await response.Content.ReadAsStringAsync();
                //var obj = JsonConvert.DeserializeObject(responseData);
            }

            var expenseItem = new CashItem()
            {
                Date = Convert.ToDateTime("05 Aug 2016"),
                CategoryId = 1,
                Amount = 40.55,
                Description = "Optus"
            };

            //Create
            //var settings = new JsonSerializerSettings();
            //settings.ContractResolver = new LowercaseContractResolver();
            //var json = JsonConvert.SerializeObject(expenseItem, Formatting.Indented, settings);

            //response = await client.PostAsync("cashitems/expenses", new StringContent(json, Encoding.UTF8,"application/json"));
            //if (response.IsSuccessStatusCode)
            //{

            //}


            //Update
            var settings = new JsonSerializerSettings();
            settings.ContractResolver = new LowercaseContractResolver();
            var json = JsonConvert.SerializeObject(expenseItem, Formatting.Indented, settings);

            response = await client.PutAsync("cashitems/4", new StringContent(json, Encoding.UTF8, "application/json"));
            if (response.IsSuccessStatusCode)
            {

            }
        }
    }
}
