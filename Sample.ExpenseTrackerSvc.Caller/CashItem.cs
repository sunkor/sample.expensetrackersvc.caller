﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.ExpenseTrackerSvc.Caller
{
    class CashItem
    {
        public int ItemID;
        public DateTime Date;
        public int CategoryId;
        public double Amount;
        public string Description;
        public bool Deleted;
        public DateTime LastUpdate;
    }
}
